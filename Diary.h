#ifndef DIARY_H
#define DIARY_H

#include "Message.h"
#include <string>
#include <vector>

struct Diary {
	vector<Message> mensagens;

	void add(const std::string& mensagem);
	void list();
	void edit(int linha);
	void remove(int linha);
	std::string get_data_atual();
	std::string get_hora_atual();
};

#endif