#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>

struct Date {
	int dia;
	int mes;
	int ano;
};

struct Time {
	int hora;
	int minuto;
	int segundo;
};

struct Message {
	Date date;
	Time time;
	std::string content;

	bool compare_dates(const Message &other_message);
};

#endif